package com.map.placeindicator.activity.map

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.map.placeindicator.activity.map.model.response.Geometry
import com.map.placeindicator.activity.map.model.response.Location
import com.map.placeindicator.activity.map.model.response.MapResponseModel
import com.map.placeindicator.activity.map.model.response.Results
import com.map.placeindicator.exception.LocationException
import com.map.placeindicator.retrofit.HttpClient
import com.map.placeindicator.utils.LocationHelper
import io.reactivex.Flowable
import junit.framework.Assert.assertEquals
import org.hamcrest.core.IsEqual.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner



@RunWith(MockitoJUnitRunner::class)
class MapPresenterTest {
    @Mock private lateinit var locationHelper: LocationHelper
    @Mock private lateinit var mapView: MapContract.View
    @Mock private lateinit var httpClient: HttpClient
    @Mock private lateinit var context: Context

    private lateinit var mapPresenter: MapPresenter

    @Before
    fun setUp() {
        mapPresenter = MapPresenter(context, locationHelper, httpClient)
        mapPresenter.attachView(mapView)
    }

    @Test
    fun shouldSetMarkerOptions() {
        `when`(locationHelper.getLatitude()).thenReturn(23.01)
        `when`(locationHelper.getLongitude()).thenReturn(15.40)

        val latLng: LatLng = mapPresenter.getMyLocation()

        assertEquals(23.01, latLng.latitude)
        assertEquals(15.40, latLng.longitude)
    }

    @Test(expected = LocationException::class)
    fun shouldThrowLocationException() {
        `when`(locationHelper.getLatitude()).thenReturn(null)
        `when`(locationHelper.getLongitude()).thenReturn(null)

        mapPresenter.getMyLocation()
    }

    @Test
    fun shouldShowSuccessResponse() {
        `when`(locationHelper.getLatitude()).thenReturn(20.2)
        `when`(locationHelper.getLongitude()).thenReturn(30.3)
        `when`(httpClient.getPlaces("20.2,30.3", "KEYWORD")).thenReturn(Flowable.just(createMockSuccessResponse()))

        mapPresenter.searchPlace("KEYWORD")

        val placesArgumentCaptor = ArgumentCaptor.forClass(Array<LatLng>::class.java)
        verify(mapView).setPlaces(placesArgumentCaptor.capture())
        assertThat(placesArgumentCaptor.value, equalTo(dumpSuccessLatLngArray()))
    }

    @Test
    fun shouldShowErrorResponse() {
        `when`(locationHelper.getLatitude()).thenReturn(20.2)
        `when`(locationHelper.getLongitude()).thenReturn(30.3)

        val response = MapResponseModel("OK", emptyArray())
        `when`(httpClient.getPlaces("20.2,30.3", "KEYWORD")).thenReturn(Flowable.just(response))

        mapPresenter.searchPlace("KEYWORD")

        verify(mapView, never()).setPlaces(any(Array<LatLng>::class.java))
        verify(mapView).showErrorSearchingPlace(null)
    }

    @After
    fun doAfter() {
        mapPresenter.detachView()
    }

    private fun createMockSuccessResponse(): MapResponseModel {
        val location_1 = Location("20.20", "30.30")
        val geometry_1 = Geometry(location_1)
        val result_1 = Results("ID", "NAME_1", geometry_1)

        val location_2 = Location("30.30", "40.40")
        val geometry_2 = Geometry(location_2)
        val result_2 = Results("ID", "NAME_2", geometry_2)

        val location_3 = Location("20.20", "30.30")
        val geometry_3 = Geometry(location_3)
        val result_3 = Results("ID", "NAME_3", geometry_3)

        val location_4 = Location("20.20", "30.30")
        val geometry_4 = Geometry(location_4)
        val result_4 = Results("ID", "NAME_4", geometry_4)

        val results = Array(4, { result_1; result_2; result_3; result_4 })
        val response = MapResponseModel("OK", results)

        return response
    }

    private fun dumpSuccessLatLngArray(): Array<LatLng> {
        return  Array(3, { LatLng(20.20, 30.30); LatLng(30.30, 40.40);LatLng(20.20, 30.30) })
    }
}
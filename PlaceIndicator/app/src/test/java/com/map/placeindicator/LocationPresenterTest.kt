package com.map.placeindicator.activity.location

import android.content.Context
import com.map.placeindicator.exception.LocationException
import com.map.placeindicator.utils.LocationHelper
import com.map.placeindicator.utils.PermissionHelper
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocationPresenterTest {
    @Mock private lateinit var permissionHelper: PermissionHelper
    @Mock private lateinit var locationHelper: LocationHelper
    @Mock private lateinit var locationView: LocationContract.View
    @Mock private lateinit var context: Context

    private lateinit var locationPresenter: LocationPresenter

    @Before
    fun setUp() {
        locationPresenter = LocationPresenter(context, permissionHelper, locationHelper)
        locationPresenter.attachView(locationView)
    }

    @Test
    fun shouldRequestForPermission() {
        `when`(permissionHelper.isPermissionGranted(anyString())).thenReturn(false)

        locationPresenter.checkLocationPermission()

        verify(locationView).requestForPermission(anyString())
    }

    @Test
    fun shouldGetLocationName() {
        `when`(permissionHelper.isPermissionGranted(anyString())).thenReturn(true)
        `when`(locationHelper.getLocationName()).thenReturn("SAMPLE_STRING")

        locationPresenter.checkLocationPermission()

        verify(locationView).setLocationName("SAMPLE_STRING")
    }

    @Test
    fun shouldShowLocationError() {
        `when`(locationHelper.getLocationName()).thenThrow(LocationException())

        locationPresenter.getLocationName()

        verify(locationView).showLocationError("Your location cannot be found")
    }

    @Test
    fun shouldShowLatitudeAndLongitudeError() {
        `when`(locationHelper.getLatitude()).thenReturn(null)
        `when`(locationHelper.getLongitude()).thenReturn(null)
        `when`(context.getString(anyInt())).thenReturn("ANY_STRING")

        locationPresenter.startSearchingTextOnClick()

        verify(locationView).showLocationError(anyString())
        verify(locationView, never()).startMapActivity()
    }

    @Test
    fun shouldStartActivityWithoutShowingError() {
        `when`(locationHelper.getLatitude()).thenReturn(23.01)
        `when`(locationHelper.getLongitude()).thenReturn(15.40)

        locationPresenter.startSearchingTextOnClick()

        verify(locationView, never()).showLocationError(anyString())
        verify(locationView).startMapActivity()
    }

    @After
    fun doAfter(){
        locationPresenter.detachView()
    }
}
package com.map.placeindicator.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import javax.inject.Inject

public class PermissionHelper @Inject constructor(private var context: Context) {

    public fun isPermissionGranted(permission: String): Boolean
            = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED

    public fun requestForPermission(activity: Activity, permission: String, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
    }
}

package com.map.placeindicator.utils

import android.content.Context
import android.widget.Toast
import javax.inject.Inject

public class ToastHelper @Inject constructor(private var context: Context) {

    fun showInformationToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}

package com.map.placeindicator.activity.map

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.inputmethod.InputMethodManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.map.placeindicator.PlaceIndicatorApplication.Companion.applicationComponent
import com.map.placeindicator.R
import com.map.placeindicator.utils.ToastHelper
import kotlinx.android.synthetic.main.activity_map.*
import org.jetbrains.anko.onClick
import javax.inject.Inject

class MapActivity : AppCompatActivity(), OnMapReadyCallback, MapContract.View {

    @Inject lateinit var presenter: MapContract.Presenter
    @Inject lateinit var toastHelper: ToastHelper

    private var map: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        applicationComponent.inject(this)

        presenter.attachView(this)
        presenter.initRecyclerAdapter()

        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

        searchingIcon.onClick {
            val searchingPlace = searchingPlaceEditText.text.toString()
            if (!TextUtils.isEmpty(searchingPlace)) {
                presenter.searchPlace(searchingPlace)

                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                if (imm.isAcceptingText) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                }
            }
        }
    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map
        presenter.getMyLocationMarker()

        val zoom = 12f
        val duration = 2000
        val cancelableCallback = null

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(presenter.getMyLocation(), zoom))
        map.animateCamera(CameraUpdateFactory.zoomIn())
        map.animateCamera(CameraUpdateFactory.zoomTo(zoom), duration, cancelableCallback)
    }

    override fun initRecyclerAdapter(adapter: PlaceListAdapter) {
        placeRecyclerView.layoutManager = LinearLayoutManager(this)
        placeRecyclerView.adapter = adapter
    }

    override fun setMarkerOptions(markerOptions: MarkerOptions) {
        map?.addMarker(markerOptions)
    }

    override fun setPlaces(places: Array<LatLng>?) {
        map?.clear()

        presenter.getMyLocationMarker()
        places?.forEach {
            map?.addMarker(MarkerOptions()
                    .position(it))
        }
    }

    override fun showErrorSearchingPlace(message: String?) {
        if (message != null)
            toastHelper.showInformationToast(message)
        else
            toastHelper.showInformationToast(getString(R.string.map_activity_searching_place_error))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}

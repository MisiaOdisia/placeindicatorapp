package com.map.placeindicator.utils

import android.content.Context
import android.location.Geocoder
import android.location.LocationManager
import com.map.placeindicator.exception.LocationException
import java.util.*
import javax.inject.Inject

public class LocationHelper @Inject constructor(private var context: Context) {
    private var longitude: Double? = null
    private var latitude: Double? = null

    public fun getLongitude(): Double? {
        return longitude
    }

    public fun getLatitude(): Double? {
        return latitude
    }

    @Throws(LocationException::class)
    public fun getLocationName(): String {
        retrieveLongitudeAndLatitude()

        if (longitude != null && latitude != null) {
            val maxResults = 1

            val geocoder: Geocoder = Geocoder(context, Locale.getDefault())
            val addresses = geocoder.getFromLocation(latitude!!, longitude!!, maxResults)

            val city = addresses[0].locality

            return city
        } else throw LocationException()
    }

    public fun retrieveLongitudeAndLatitude() {
        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        longitude = location.longitude
        latitude = location.latitude
    }
}

package com.map.placeindicator.dagger

@MustBeDocumented
@javax.inject.Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity

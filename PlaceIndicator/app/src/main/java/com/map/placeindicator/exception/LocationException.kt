package com.map.placeindicator.exception

class LocationException : Exception() {
    override val message: String
        get() = "Your location cannot be found"
}
package com.map.placeindicator.activity.location

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.map.placeindicator.PlaceIndicatorApplication.Companion.applicationComponent
import com.map.placeindicator.R
import com.map.placeindicator.activity.map.MapActivity
import com.map.placeindicator.utils.PermissionHelper
import com.map.placeindicator.utils.ToastHelper
import kotlinx.android.synthetic.main.activity_location.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.onClick
import javax.inject.Inject


class LocationActivity : AppCompatActivity(), LocationContract.View {
    companion object {
        private val PERMISSION_REQUEST_CODE = 1
    }

    @Inject lateinit var presenter: LocationContract.Presenter
    @Inject lateinit var permissionHelper: PermissionHelper
    @Inject lateinit var toastHelper: ToastHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        applicationComponent.inject(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
        presenter.checkLocationPermission()

        startSearchingTextView.onClick { presenter.startSearchingTextOnClick() }
    }

    override fun requestForPermission(permission: String) {
        permissionHelper.requestForPermission(this, permission, PERMISSION_REQUEST_CODE)
    }

    override fun setLocationName(location: String) {
        locationTextView.text = location
    }

    override fun showLocationError(errorMessage: String) {
        toastHelper.showInformationToast(errorMessage)
    }

    override fun startMapActivity() {
        startActivity(intentFor<MapActivity>())
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED))
                    presenter.getLocationName()
                else {
                    toastHelper.showInformationToast(getString(R.string.localization_activity_permission_denied_message))
                    locationTextView.text = getString(R.string.location_activity_access_danied)
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }
}

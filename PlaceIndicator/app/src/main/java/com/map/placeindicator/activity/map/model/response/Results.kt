package com.map.placeindicator.activity.map.model.response

data class Results(var id: String?, var name: String?, var geometry: Geometry?)
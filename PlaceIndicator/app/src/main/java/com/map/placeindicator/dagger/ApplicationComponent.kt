package com.map.placeindicator.dagger

import com.map.placeindicator.activity.location.LocationActivity
import com.map.placeindicator.activity.location.LocationContract
import com.map.placeindicator.activity.map.MapActivity
import com.map.placeindicator.activity.map.MapContract
import com.map.placeindicator.dagger.module.MainModule
import com.map.placeindicator.dagger.module.PresenterModule
import com.map.placeindicator.retrofit.HttpClient
import com.map.placeindicator.utils.LocationHelper
import com.map.placeindicator.utils.PermissionHelper

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(MainModule::class, PresenterModule::class))
interface ApplicationComponent {
    fun providesPermissionHelper(): PermissionHelper
    fun providesLocationHelper(): LocationHelper
    fun providesHttpClient(): HttpClient
    fun providesLocationPresenter(): LocationContract.Presenter
    fun providesMapPresenter(): MapContract.Presenter

    fun inject(locationActivity: LocationActivity)
    fun inject(mapActivity: MapActivity)
}

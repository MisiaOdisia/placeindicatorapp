package com.map.placeindicator.activity.map.model.response

data class MapResponseModel(var status: String?, var results: Array<Results>)

package com.map.placeindicator.dagger

import android.app.Application
import com.map.placeindicator.dagger.module.MainModule
import com.map.placeindicator.dagger.module.PresenterModule

open class DaggerApplication : Application() {

    protected fun provideApplicationComponent(): ApplicationComponent {
        return DaggerApplicationComponent.builder()
                .mainModule(MainModule(this))
                .presenterModule(PresenterModule())
                .build()
    }
}

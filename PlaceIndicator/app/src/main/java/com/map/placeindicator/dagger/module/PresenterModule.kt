package com.map.placeindicator.dagger.module

import android.content.Context
import com.map.placeindicator.activity.location.LocationContract
import com.map.placeindicator.activity.location.LocationPresenter
import com.map.placeindicator.activity.map.MapContract
import com.map.placeindicator.activity.map.MapPresenter
import com.map.placeindicator.retrofit.HttpClient
import com.map.placeindicator.utils.LocationHelper
import com.map.placeindicator.utils.PermissionHelper

import dagger.Module
import dagger.Provides

@Module
class PresenterModule {

    @Provides
    fun providesLocationPresenter(context: Context, permissionHelper: PermissionHelper, locationHelper: LocationHelper): LocationContract.Presenter
            = LocationPresenter(context, permissionHelper, locationHelper)

    @Provides
    fun providesMapPresenter(context: Context, locationHelper: LocationHelper, httpClient: HttpClient): MapContract.Presenter
            = MapPresenter(context, locationHelper, httpClient)
}

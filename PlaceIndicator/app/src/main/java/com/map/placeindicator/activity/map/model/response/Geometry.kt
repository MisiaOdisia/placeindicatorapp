package com.map.placeindicator.activity.map.model.response

data class Geometry(var location: Location?)
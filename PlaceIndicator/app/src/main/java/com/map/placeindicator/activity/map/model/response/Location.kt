package com.map.placeindicator.activity.map.model.response

data class Location(var lat: String?, var lng: String?)

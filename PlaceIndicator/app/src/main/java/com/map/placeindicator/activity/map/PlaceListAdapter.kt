package com.map.placeindicator.activity.map

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.map.placeindicator.R
import com.map.placeindicator.activity.map.model.response.Results
import kotlinx.android.synthetic.main.place_item.view.*

class PlaceListAdapter(var places: Array<Results>) : RecyclerView.Adapter<PlaceListAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder?, position: Int)
            = holder!!.bind(places[position])

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.place_item, parent, false))

    override fun getItemCount(): Int = places.size

    fun addNewPlaceList(listFromDatabase: Array<Results>) {
        this.places = listFromDatabase
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(result: Results) = with(itemView) {
            itemView.placeItemTextView.text = result.name
        }
    }
}
package com.map.placeindicator.activity.map

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.map.placeindicator.activity.base.IBasePresenter
import com.map.placeindicator.activity.base.IBaseView

interface MapContract {

    interface View : IBaseView {
        fun setMarkerOptions(markerOptions: MarkerOptions)
        fun setPlaces(places: Array<LatLng>?)
        fun showErrorSearchingPlace(message: String? = null)
        fun initRecyclerAdapter(adapter: PlaceListAdapter)
    }

    interface Presenter : IBasePresenter<View> {
        fun getMyLocationMarker()
        fun searchPlace(searchingPlace: String)
        fun getMyLocation(): LatLng
        fun initRecyclerAdapter()
    }
}
package com.map.placeindicator.dagger.module

import android.app.Application
import android.content.Context
import com.map.placeindicator.retrofit.HttpClient
import com.map.placeindicator.utils.LocationHelper
import com.map.placeindicator.utils.PermissionHelper
import com.map.placeindicator.utils.ToastHelper

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainModule(val app: Application) {

    val context: Context
        @Provides
        get() = app.applicationContext

    @Singleton
    @Provides
    fun providesPermissionHelper(): PermissionHelper = PermissionHelper(context)

    @Singleton
    @Provides
    fun providesLocationHelper(): LocationHelper = LocationHelper(context)

    @Singleton
    @Provides
    fun providesToastHelper(): ToastHelper = ToastHelper(context)

    @Singleton
    @Provides
    fun providesHttpClient(): HttpClient = HttpClient()
}

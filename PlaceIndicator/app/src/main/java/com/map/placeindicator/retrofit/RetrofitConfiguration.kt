package com.map.placeindicator.retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory



class RetrofitConfiguration {

    companion object {
        val ENDPOINT = "https://maps.googleapis.com/maps/api/"
    }

    val retrofitService: RetrofitService
        get() {
            return createRetrofitService().create(RetrofitService::class.java)
        }

    private fun createRetrofitService(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ENDPOINT)
                .build()
    }
}


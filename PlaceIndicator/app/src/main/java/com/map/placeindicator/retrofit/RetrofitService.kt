package com.map.placeindicator.retrofit

import com.map.placeindicator.activity.map.model.response.MapResponseModel
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    @GET("place/nearbysearch/json?radius=10000&key=AIzaSyDf5Or_ZTsMhjBN_RsJUeKtYwwX_tteJsg")
    fun getPlaces(@Query("location") location: String, @Query("keyword") type:String): Flowable<MapResponseModel>

}
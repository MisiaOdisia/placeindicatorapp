package com.map.placeindicator.activity.location

import com.map.placeindicator.activity.base.IBasePresenter
import com.map.placeindicator.activity.base.IBaseView

interface LocationContract {

    interface View : IBaseView {
        fun requestForPermission(permission: String)
        fun setLocationName(location: String)
        fun showLocationError(errorMessage: String)
        fun startMapActivity()
    }

    interface Presenter : IBasePresenter<View> {
        fun checkLocationPermission()
        fun getLocationName()
        fun startSearchingTextOnClick()
    }
}
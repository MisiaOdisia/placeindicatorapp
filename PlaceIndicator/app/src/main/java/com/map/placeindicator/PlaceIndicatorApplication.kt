package com.map.placeindicator

import android.content.Context
import com.map.placeindicator.dagger.ApplicationComponent
import com.map.placeindicator.dagger.DaggerApplication
import kotlin.properties.Delegates

public class PlaceIndicatorApplication : DaggerApplication() {

    companion object {
        var applicationComponent: ApplicationComponent by Delegates.notNull()
        var context: Context by Delegates.notNull()
    }

    init {
        context = this
    }

    override fun onCreate() {
        applicationComponent = provideApplicationComponent()
    }
}


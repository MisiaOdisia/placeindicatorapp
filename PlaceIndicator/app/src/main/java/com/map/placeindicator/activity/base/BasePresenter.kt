package com.map.placeindicator.activity.base

open class BasePresenter<in V: IBaseView> : IBasePresenter<V> {

    private var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}

package com.map.placeindicator.activity.location

import android.Manifest
import android.content.Context
import com.map.placeindicator.R

import com.map.placeindicator.activity.base.BasePresenter
import com.map.placeindicator.exception.LocationException
import com.map.placeindicator.utils.LocationHelper
import com.map.placeindicator.utils.PermissionHelper

import javax.inject.Inject

class LocationPresenter @Inject constructor(private var context: Context, private var permissionHelper: PermissionHelper,
                                            private var locationHelper: LocationHelper) : BasePresenter<LocationContract.View>(), LocationContract.Presenter {

    private var locationView: LocationContract.View? = null

    override fun attachView(view: LocationContract.View) {
        locationView = view
    }

    override fun checkLocationPermission() {
        if (!permissionHelper.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION))
            locationView?.requestForPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        else
            getLocationName()
    }

    override fun getLocationName() {
        try {
            locationView?.setLocationName(locationHelper.getLocationName())
        } catch (exception: LocationException) {
            locationView?.showLocationError(exception.message)
        }
    }

    override fun startSearchingTextOnClick() {
        if ((locationHelper.getLatitude() == null) && (locationHelper.getLongitude() == null))
            locationView?.showLocationError(context.getString(R.string.localization_activity_null_localization_error_message))
        else
            locationView?.startMapActivity()
    }

    override fun detachView() {
        this.locationView = null
    }
}
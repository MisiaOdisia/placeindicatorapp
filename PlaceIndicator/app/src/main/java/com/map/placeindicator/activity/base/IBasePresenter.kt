package com.map.placeindicator.activity.base

interface IBasePresenter<in V : IBaseView> {

    fun attachView(view: V)
    fun detachView()
}

package com.map.placeindicator.retrofit

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class HttpClient {

    private val service: RetrofitService = RetrofitConfiguration().retrofitService

    fun getPlaces(location: String, keyword: String) =
            service.getPlaces(location, keyword)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
}
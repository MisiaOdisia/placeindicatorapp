package com.map.placeindicator.activity.map

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.map.placeindicator.R
import com.map.placeindicator.exception.LocationException
import com.map.placeindicator.retrofit.HttpClient
import com.map.placeindicator.utils.LocationHelper
import timber.log.Timber
import javax.inject.Inject

class MapPresenter @Inject constructor(private var context: Context, private var locationHelper: LocationHelper,
                                       private var httpClient: HttpClient) : MapContract.Presenter {

    private var mapView: MapContract.View? = null
    private lateinit var adapter: PlaceListAdapter

    override fun attachView(view: MapContract.View) {
        mapView = view
    }

    override fun getMyLocationMarker() {
        try {
            val markerOptions = MarkerOptions()
                    .position(getMyLocation())
                    .icon(bitmapDescriptorFromVector(R.drawable.ic_my_location_on_map))

            mapView?.setMarkerOptions(markerOptions)
        } catch (exception: LocationException) {
            mapView?.showErrorSearchingPlace(exception.message)
            Timber.e(exception)
        }
    }

    override fun initRecyclerAdapter() {
        adapter = PlaceListAdapter(emptyArray())
        mapView?.initRecyclerAdapter(adapter)
    }

    override fun searchPlace(searchingPlace: String) {
        val latitude = locationHelper.getLatitude()
        val longitude = locationHelper.getLongitude()

        httpClient.getPlaces(String.format("%s,%s", latitude, longitude), searchingPlace)
                .subscribe({ response ->
                    if (!response.results.isEmpty()) {
                        mapView?.setPlaces(
                                response.results.take(3).map {
                                    LatLng(it.geometry?.location?.lat!!.toDouble(), it.geometry?.location?.lng!!.toDouble())
                                }.toTypedArray())

                        adapter.addNewPlaceList(response.results.take(3).toTypedArray())
                        adapter.notifyDataSetChanged()
                    } else mapView?.showErrorSearchingPlace()
                }, { exception ->
                    mapView?.showErrorSearchingPlace()
                    Timber.e(exception)
                })
    }

    @Throws(LocationException::class)
    override fun getMyLocation(): LatLng {
        val latitude = locationHelper.getLatitude()
        val longitude = locationHelper.getLongitude()

        if (latitude != null && longitude != null)
            return LatLng(latitude, longitude)
        else throw LocationException()
    }

    private fun bitmapDescriptorFromVector(vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun detachView() {
        this.mapView = null
    }
}
